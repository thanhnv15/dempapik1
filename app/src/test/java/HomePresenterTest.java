import com.nac.demoapi.entities.ListEmployeeEntity;
import com.nac.demoapi.presenter.HomePresenter;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.net.HttpURLConnection;

import okhttp3.mockwebserver.MockResponse;
import retrofit2.Response;

public class HomePresenterTest extends BaseTest {
    private HomePresenter presenter;

    @Override
    public void setUp() throws Exception {
        super.setUp();

        presenter = Mockito.spy(HomePresenter.class);
    }

    @Test
    public void testCalcSuccess() {
        double rs1 = presenter.calc(1, 5, 6); //plus
        double rs2 = presenter.calc(2, 5, 6); //minus
        double rs3 = presenter.calc(3, 5, 6); //multiple
        double rs4 = presenter.calc(4, 5, 2); //div # 0

        Assert.assertEquals(11, rs1, 0);
        Assert.assertEquals(-1, rs2, 0);
        Assert.assertEquals(30, rs3, 0);
        Assert.assertEquals(2.5, rs4, 0);
        try {
            presenter.calc(4, 5, 0); //div = 0
        } catch (Exception e) {
            assert (true);
            e.printStackTrace();
        }
    }

    @Test
    public void testInvalidKeyCalc(){
        double rs1 = presenter.calc(-1, 5, 6); //invalid key
        Assert.assertEquals(-1, rs1, 0);
    }

    @Test
    public void getAllEmployeeSuccess() {
        mockWebServer.enqueue(new MockResponse()
                .setResponseCode(HttpURLConnection.HTTP_OK)
                .setBody(getTextFile("GetAllEmployee.txt")));

        Response<ListEmployeeEntity> data = presenter.getAllEmployee();

        boolean rs = data != null && data.code() == HttpURLConnection.HTTP_OK && data.body() != null;
        if (rs) {
            System.out.println(data.body().toString());
        }
        assert (rs);
    }
}

