package com.nac.demoapi.api;

import com.nac.demoapi.entities.ListEmployeeEntity;

import retrofit2.Call;
import retrofit2.http.GET;

public interface IEmployeeAPI {
    @GET("employees")
    Call<ListEmployeeEntity> getAllEmployee();
}
