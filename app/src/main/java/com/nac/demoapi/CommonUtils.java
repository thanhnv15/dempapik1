package com.nac.demoapi;

public class CommonUtils {
    public static final int LOG_TYPE_TEST = 1;
    public static final int LOG_TYPE_INFO = 2;
    public static final int LOG_TYPE_DEBUG = 3;

    private static int logType = LOG_TYPE_INFO;

    public static void setLogType(int logType) {
        CommonUtils.logType = logType;
    }

    public static void logI(String tag, String msg) {
        if (logType == LOG_TYPE_INFO) {
            CommonUtils.logI(tag, msg);
        } else if (logType == LOG_TYPE_TEST) {
            System.out.println(tag + ":" + msg);
        }
    }

    public static void logD(String tag, String msg) {
        if (logType == LOG_TYPE_DEBUG) {
            CommonUtils.logI(tag, msg);
        } else if (logType == LOG_TYPE_TEST) {
            System.out.println(tag + ":" + msg);
        }
    }
}
