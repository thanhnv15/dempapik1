package com.nac.demoapi.presenter;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public abstract class BasePresenter {
    private static final String TAG = BasePresenter.class.getName();
    private static final String BASE_URL = "http://dummy.restapiexample.com/api/v1/";

    public BasePresenter() {
        initAPI();
    }

    private void initAPI() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(new OkHttpClient.Builder()
                        .callTimeout(30, TimeUnit.SECONDS)
                        .hostnameVerifier((hostname, session) -> true)
                        .build())
                .build();

        //Tạo request từ interface
        createServiceAPI(retrofit);
    }

    protected abstract void createServiceAPI(Retrofit retrofit);
}
