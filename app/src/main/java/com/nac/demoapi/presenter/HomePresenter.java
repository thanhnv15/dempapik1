package com.nac.demoapi.presenter;

import com.nac.demoapi.CommonUtils;
import com.nac.demoapi.api.IEmployeeAPI;
import com.nac.demoapi.entities.ListEmployeeEntity;

import java.io.IOException;

import retrofit2.Response;
import retrofit2.Retrofit;

public class HomePresenter extends BasePresenter {
    private static final String TAG = HomePresenter.class.getName();
    private IEmployeeAPI mEmployeeAPI;

    public double calc(int key, int a, int b) {
        if (key == 1) {
            return a + b;
        } else if (key == 2) {
            return a - b;
        } else if (key == 3) {
            return a * b;
        } else if (key == 4) {
            return a * 1.0 / b;
        }
        return -1;
    }

    public Response<ListEmployeeEntity> getAllEmployee() {
        CommonUtils.logI(TAG, "getAllEmployee...");
        try {
            return mEmployeeAPI.getAllEmployee().execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void createServiceAPI(Retrofit retrofit) {
        CommonUtils.logI(TAG, "createServiceAPI...");
        mEmployeeAPI = retrofit.create(IEmployeeAPI.class);
    }
}
