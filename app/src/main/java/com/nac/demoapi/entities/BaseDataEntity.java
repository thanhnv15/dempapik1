package com.nac.demoapi.entities;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BaseDataEntity implements Serializable {
    @SerializedName("status")
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
